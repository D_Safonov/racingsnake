﻿using UnityEngine;
using System.Collections;

public class gameManagerControll : MonoBehaviour {
	
	public UILabel infoLabel;
	public enum gameState
		{
			GameMenu,
			GameStart,
			GameOver
		}
	public static gameManagerControll link;
	public gameState game;
	public GameObject menuPanel;
	public GameObject restartPanel;
	public GameObject uiPanel;
	public GameObject mainCamera;
	public GameObject uiCamera;
	public GameObject car;
	public GameObject spawner;
	public GameObject timer;

	public UILabel ballsCounterLabel;
	public UILabel WinLoseLabel;

	private int ballsCounter = 0;

	public float accelPressed;
	public bool sprintPressed;
	public float steerCoeff;

	// Use this for initialization
	void Start () {
		link = this.GetComponent<gameManagerControll> ();
		game = gameState.GameMenu;

	}
	
	// Update is called once per frame
	void Update () {
		if (game == gameState.GameOver) 
		{
			restartPanel.SetActive(true);	
			menuPanel.SetActive(false);
			uiCamera.SetActive(true);
			uiPanel.SetActive(false);
		}
		if (game == gameState.GameStart) 
		{
			restartPanel.SetActive(false);
			menuPanel.SetActive(false);
		}
		if (game == gameState.GameMenu) {
						menuPanel.SetActive (true);	
				}
		}
	
	public void startGame()
	{
		mainCamera.GetComponent<VehicleCamera> ().enabled = true;
		mainCamera.GetComponent<rotateAround> ().enabled = false;
		uiPanel.SetActive (true);
		game = gameState.GameStart;
		StartCoroutine ("showInfoMessage");
		spawner.GetComponent<BallSpawner>().DeleteBalls();
		spawner.GetComponent<BallSpawner>().SpawnBalls();
	}

	public void IncBallCount()
	{
		ballsCounterLabel.text = string.Format("{0}/30",(++ballsCounter).ToString());
		if(ballsCounter % 5 == 0)
		{
			//spawner.GetComponent<BallSpawner>().SpawnBalls();
		}
		timer.GetComponent<timerScript>().eatBall();
	
		if(ballsCounter == 30)
		{
			WinLoseLabel.text = "Cogratulation!!!!You WIN!";
			game = gameState.GameOver;
		}
	}

	public void GasPressed()
	{
		accelPressed = 2000.0f;

	}

	public void GasReleased()
	{
		accelPressed = 0.0f;
	}

	public void BreakPressed()
	{
		accelPressed = -10.0f;
	}

	public void BreakReleased()
	{
		accelPressed = 0.0f;
	}

	public void LeftPressed()
	{
		steerCoeff = -0.5f;
	}

	public void TurnReleased()
	{
		steerCoeff = 0.0f;
	}

	public void RightPressed()
	{
		steerCoeff = 0.5f;
	}

	public void SprintPressed()
	{
		sprintPressed = true;
	}

	public void SprintReleased()
	{
		sprintPressed = false;
	}

	IEnumerator showInfoMessage()
	{
		yield return new WaitForSeconds (3.0f);
		infoLabel.enabled = false;
	}
	
}
