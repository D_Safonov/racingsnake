﻿using UnityEngine;
using System.Collections;

public class rotatePoint : MonoBehaviour {
	private Vector3 curPos;
	// Use this for initialization
	void Start () {
		curPos = this.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.localEulerAngles += new Vector3 (0.0f, 1.0f, 0.0f);
		this.transform.localPosition += new Vector3 (0.0f, 0.0f, 0.1f);
		if (this.transform.localPosition.z > 200) {
			this.transform.localPosition += new Vector3 (0.1f, 0.0f, 0.1f);
		}
		if (this.transform.localPosition.z > 400) {
			this.transform.localPosition = curPos;
		}
	}
}
