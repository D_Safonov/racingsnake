﻿using UnityEngine;
using System.Collections;
using vechGui;
public class BallSpawner : MonoBehaviour {

	public int numberOfSpawnedBalls = 5;
	public int numberOfSpawnedCars = 20;
	public GameObject ballPrefab;
	public GameObject oldCarPrefab;
	public float[] spawnLimits =new float[4];

	// Use this for initialization
	void Start () {
		SpawnBalls();
		SpawnCars();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SpawnBalls()
	{
		for (int i = 0; i < numberOfSpawnedBalls;++i)
		{
			float initx =383 + Random.Range(spawnLimits[0], spawnLimits[1]);
			float initz =-97 + Random.Range(spawnLimits[2], spawnLimits[3]);

			Instantiate(ballPrefab, new Vector3(initx,1,initz), new Quaternion());
		}
	}

	public void SpawnCars()
	{
			for (int i = 0; i < numberOfSpawnedCars; ++i)
			{
				float initx = 383 + Random.Range(spawnLimits[0], spawnLimits[1]);
				float initz = -97 + Random.Range(spawnLimits[2], spawnLimits[3]);
				
				var go = (GameObject)Instantiate(oldCarPrefab, new Vector3(initx,3,initz), new Quaternion());
				go.GetComponent<VehicleControl>().enabled = false;
				go.GetComponent<VehicleDamage>().enabled = false;
				go.GetComponent<VehicleGUI>().enabled = false;
			}
	}

	public void DeleteBalls()
	{
		var objects = GameObject.FindObjectsOfType<BallScript>();

		foreach(var ball in objects)
		{
			Destroy(ball.gameObject);
		}
	}
}
