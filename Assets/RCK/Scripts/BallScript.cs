﻿using UnityEngine;
using System.Collections;

public class BallScript : MonoBehaviour {

	private gameManagerControll gmControl;
	void Start()
	{
		gmControl = GameObject.Find("gameManager").GetComponent<gameManagerControll>();

	}

	void OnTriggerEnter(Collider coll)
	{
		if(coll.gameObject.tag == "Player")
		{
			//gameObject.SetActive(false);
			Destroy(gameObject);
			gmControl.IncBallCount();
		}
	}
}
