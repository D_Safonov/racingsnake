﻿using UnityEngine;
using System.Collections;

public class timerScript : MonoBehaviour {
	float time = 25.0f;
	string currentTime;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		time -= Time.deltaTime;
		currentTime = string.Format ("{0:0.0}", time);
		this.GetComponent<UILabel> ().text = currentTime;
		EndGame ();
	}
	public void EndGame()
	{
		if (time <= 10.0f) {
			this.GetComponent<UILabel> ().color = Color.red;
		}
		if (time < 0) {
			gameManagerControll.link.game = gameManagerControll.gameState.GameOver;
			GameObject.FindObjectOfType<gameManagerControll>().WinLoseLabel.text = "TRY AGAIN!";
		}
	}
	public void eatBall()
	{
		time += 20.0f;
	}
}
